<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/styles/default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/highlight.min.js"></script>
<script>hljs.highlightAll();</script>

# COMPTE RENDU DE LA PARTIE 2 DE LA SAE2.02 

## Question 2:

## Compte-rendu de complexité de l'algorithme algo1.

```c
void algo1 (int tab [], int nb ){
    int echange = 1;
    int i;
    int deb = 0, fin = nb -1;

    while (echange >0){
        echange =0;
        for(i=deb; i < fin; i++){
            if(tab[i]> tab[i +1]){
                echanger (tab , i,i+1);
                echange ++;
            }
        }

        fin --;
        for(i=fin; i > deb; i--)
        {
            if(tab[i]< tab[i -1]){
                echanger (tab , i,i -1);
                echange ++;
            }
        }
        deb ++;
    }
}

void echanger (int tab [], int i, int j){
    int tmp;
    tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}
```

# Analyse de la complexité
### Boucle while

La boucle `while` s'exécute tant que la variable `echange` est supérieure à 0. Cette variable est initialisée à 1 avant le début de la boucle, ce qui garantit que la boucle s'exécute au moins une fois.

Le corps de la boucle contient deux boucles `for` qui trient le tableau, l'une en parcourant le tableau de gauche à droite, l'autre en parcourant le tableau de droite à gauche. La complexité de chaque boucle `for` est de O(n), où n est le nombre d'éléments dans le tableau.

Le nombre total d'itérations de la boucle `while` dépend de la disposition des éléments dans le tableau. Dans le pire des cas, le tableau est trié dans l'ordre inverse, ce qui entraîne un nombre maximal d'échanges à chaque itération de la boucle `while`. Dans ce cas, la boucle `while` effectue n itérations (car il y a n éléments dans le tableau) et chaque itération de la boucle `while` effectue deux boucles `for` de O(n) itérations chacune. Ainsi, la complexité totale de la boucle `while` dans le pire des cas est de O(n²).

### Fonction echanger

La fonction `echanger` a une complexité de O(1), car elle effectue un nombre constant d'opérations (3 affectations de variables) quel que soit la taille du tableau.

### Conclusion

La complexité de l'algorithme algo1 dépend de la disposition des éléments dans le tableau. Dans le pire des cas, l'algorithme a une complexité de O(n²). La complexité moyenne est de O(n²), ce qui en fait un algorithme relativement lent pour trier de grands tableaux.

## Compte rendu de l'algorithme algo2

```c
void algo2(int tab [], int nb ){
    int* cpt;
    int min = tab [0] , max = tab [0];
    int i,j;
    for(i=1; i<nb; i++){
        if(min > tab[i]) min = tab[i];
        if(max < tab[i]) max = tab[i];
    }
    cpt = (int *) calloc ((max -min +1) , sizeof (int ));
    if(cpt == NULL ){
        printf ("pb aloc mémoire \n");
        exit (1);
    }
    for(i = 0; i<nb; i++){
        cpt[tab[i]-min ]++;
    }

    j=0;
    for(i=0; i< max -min +1 ; i++){
        while (cpt[i ]!=0){
            tab[j] = i+min;
            j++;
            cpt[i]--;
        }
    }
    free(cpt );
}
```


# Analyse de la complexité :

### Première boucle for
La fonction `algo2` utilise l'algorithme de tri par comptage pour trier un tableau `tab` de taille `nb`. 
Tout d'abord, elle parcourt le tableau pour trouver les valeurs minimales et maximales, ce qui prend une complexité de 
O(n). Ensuite, elle alloue de la mémoire pour un tableau de comptage `cpt` de taille `max-min+1`, ce qui prend une complexité de O(max), où max est la plage de valeurs possibles dans le tableau.

### Deuxième boucle for
Ensuite, elle parcourt le tableau `tab` une deuxième fois pour incrémenter les compteurs correspondants dans le tableau de comptage `cpt`, ce qui prend une complexité de O(n). Enfin, elle parcourt le tableau de comptage cpt pour réinsérer les valeurs triées dans le tableau d'origine tab. Cela prend une complexité de O(max), où max est la plage de valeurs possibles dans le tableau.

### Conclusion
Ainsi, la complexité totale de la fonction `algo2` est de O(n+max), où n est la taille du tableau et max est la plage de valeurs possibles dans le tableau. Le tri par comptage est efficace est efficace pour des tableaux a grande valeurs.

## Compte rendu de l'algorithme algo3
```c
void algo3 (int tab [], int n){
    int i, pos , j, tmp;
    for(i = 1; i< n; i++){
        pos = recherchePos (tab ,i, tab[i]);
        if(pos !=i){
            tmp = tab[i];
            for(j = i; j> pos; j--){
                tab[j] = tab[j -1];
            }
            tab[pos] = tmp;
        }
    }
}

int recherchePos (int tab [], int n, int val ){
    int i;
    for(i = 0; i<n; i++){
        if(val < tab[i]){
            return i;
        }
    }
    return i;
}
```

# Analyse de la complexité:

### Première boucle for :

La boucle `for` principale parcourt les éléments du tableau `tab` de l'indice 1 jusqu'à l'indice n-1. Ainsi, le nombre d'itérations de cette boucle est n-1. À chaque itération de cette boucle, la fonction `recherchePos` est appelée, qui a une complexité de O(n) dans le pire des cas. Par conséquent, la complexité de cette partie de l'algorithme est de O(n²) dans le pire des cas.

### Boucle for interne :
Dans la boucle intérieure, l'appel à la fonction recherchePos prend un temps proportionnel à i pour chaque élément i du tableau. Ainsi, le coût temporel total de cette boucle est :
O(1) + O(2) + O(3) + ... + O(n-1)

Cette somme peut être simplifiée en utilisant la formule de la somme arithmétique :
O(1) + O(2) + O(3) + ... + O(n-1) = (n-1)(n-2)/2

### Fonction recherchePos :
La fonction `recherchePos` a une complexité de O(n) dans le pire des cas, car elle parcourt le tableau `tab` jusqu'à trouver une position où la valeur `val` doit être insérée.

### Conclusion :
Cela donne un coût temporel de O(n^2) pour la boucle intérieure.
La complexité totale de l'algorithme est donc la somme des coûts temporels des deux boucles, soit O(n) + O(n²) = O(n²) dans le pire des cas.

## Conclusion sur la meilleur complexité théorique :
On peut donc conclure que l’algorithme de tri avec la meilleure complexité théorique est l’algo numéro 2, le tri par comptage car avec une complexité de O(n+max) il est bien plus performant avec des tableaux ayant des très grandes valeurs.

## Question 4 :
## Résumé algo 1: 
L'algorithme `algo1` est un algorithme de tri appelé `Tri Cocktail`. Il s'agit d'une variante du Tri à bulles qui permet de trier un tableau en le parcourant dans les deux sens.

1. Avantages :
    - L'algorithme Tri Cocktail est plus efficace que le Tri à bulles car il peut détecter des éléments inversés dans le tableau dans les deux sens.
    - Cet algorithme peut être plus rapide que d'autres algorithmes de tri pour des tableaux déjà partiellement triés.

2. Inconvénients :
    - Le Tri Cocktail peut être moins performant que d'autres algorithmes de tri pour des tableaux désordonnés.
    - Il peut être plus complexe à implémenter et à comprendre que d'autres algorithmes de tri.
    - De plus sur des tableaux très long, il n’est pas performant du fait de sa complexité de O(n²)

## Résumé algo 2: 
L'algorithme `algo2` est un algorithme de tri appelé `Tri par comptage`. Il consiste à dénombrer le nombre d'occurrences de chaque élément du tableau à trier pour ensuite réécrire le tableau dans l'ordre.

1. Avantages :
    - Le tri par comptage est très efficace pour trier de grandes quantités de données. Il est souvent beaucoup plus rapide que d'autres méthodes de tri, telles que le tri à bulles ou le tri fusion.
2. Inconvénients :
    - Peut être plus complexe à comprendre que d’autres algorithme de tri tel que le tri par insertion 
    - Requiert un espace mémoire suffisant : Le tri par comptage nécessite un espace mémoire suffisant pour stocker les compteurs. 
    - Si l'espace mémoire n'est pas suffisant, le tri par comptage ne peut pas être utilisé pour trier les données.


## Résumé algo 3: 
L'algorithme `algo3` est un algorithme de tri appelé `Tri par insertion`. Il consiste à insérer chaque élément du tableau à sa place dans un sous-tableau trié.

1. Avantages :
    - L'algorithme de tri par insertion est efficace pour les tableaux de petite taille ou déjà partiellement triés.
    - Il est facile à comprendre et à implémenter.
2. Inconvénients :
    - L'algorithme de tri par insertion peut être inefficace pour les tableaux de grande taille ou désordonnés.
    - Du fait de sa complexité temporelle de O(n²), il peut être très lent à trier pour les tableaux de grande taille.
