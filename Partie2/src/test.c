// #include "algo1.h"

// void test_1(){
//     int tab[6] = {4,3,2,1,5,4}; 
//     int nb = 6;

//     clock_t debut = clock(); // Début du chronomètre
//     algo1(tab, nb); // Appel de la fonction à tester
//     clock_t fin = clock(); // Fin du chronomètre
//     double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution 

//     printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution


//     int i; 
//     for(i=0; i<nb; i++){ 
//         printf("%d \n", tab[i]); // Affichage du tableau trié
//     }
// }

// void test_2() {
//     int tab[6] = {4, 3, 2, 1, 5, 8};
//     int nb = 6;

//     clock_t debut = clock(); // Début du chronomètre
//     algo2(tab, nb); // Appel de la fonction à tester
//     clock_t fin = clock(); // Fin du chronomètre
//     double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution

//     printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution

//     int i;
//     for (i = 0; i < nb; i++) { 
//         printf("%d \n", tab[i]); // Affichage du tableau trié
//     }
// }

// void test_3(){
//     int tab[6] = {4,2,3,1,5,9};
//     int nb = 6;

//     clock_t debut = clock(); // Début du chronomètre
//     algo3(tab, nb); // Appel de la fonction à tester
//     clock_t fin = clock();  // Fin du chronomètre
//     double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution

//     printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution


//     int i;
//     for(i=0; i<nb; i++){
//         printf("%d \n", tab[i]); // Affichage du tableau trié
//     }
// }

// int main(){
//     //test_1();
//     //test_2();
//     //test_3();
//     return 0;
// }