#include "algo1.h"

void test_1() {
    int nb;
    printf("Entrez le nombre d'éléments du tableau : ");
    scanf("%d", &nb);

    int tab[nb];

    // Génère un tableau d'éléments avec des nombres aléatoires entre 1 et 100
    srand(time(NULL));
    for (int i = 0; i < nb; i++) {
        tab[i] = rand() % 100 + 1;
    }

    clock_t debut = clock(); // Début du chronomètre
    algo1(tab, nb); // Appel de la fonction à tester
    clock_t fin = clock(); // Fin du chronomètre
    double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution

    printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution
}


void test_2() {
    int nb;
    printf("Entrez le nombre d'éléments du tableau : ");
    scanf("%d", &nb);

    int tab[nb];

    // Génère un tableau d'éléments avec des nombres aléatoires entre 1 et 100
    srand(time(NULL)); 
    for (int i = 0; i < nb; i++) {
        tab[i] = rand() % 100 + 1;
    }

    clock_t debut = clock(); // Début du chronomètre
    algo2(tab, nb); // Appel de la fonction à tester
    clock_t fin = clock(); // Fin du chronomètre
    double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution

    printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution
}

void test_3(){
    int nb;
    printf("Entrez le nombre d'éléments du tableau : ");
    scanf("%d", &nb);

    int tab[nb];

    // Génère un tableau d'éléments avec des nombres aléatoires entre 1 et 100
    srand(time(NULL)); 
    for (int i = 0; i < nb; i++) {
        tab[i] = rand() % 100 + 1;
    }

    clock_t debut = clock(); // Début du chronomètre
    algo3(tab, nb); // Appel de la fonction à tester
    clock_t fin = clock(); // Fin du chronomètre
    double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution 

    printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution
}

int main(){
    //test_1();
    test_2();
    //test_3();
    return 0;
}