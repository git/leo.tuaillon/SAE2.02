//
// Created by matheo on 20/03/23.
//

#include "algo1.h"

void algo1 (int tab [], int nb ){
    int echange = 1;
    int i;
    int deb = 0, fin = nb -1;

    while (echange >0){
        echange =0;
        for(i=deb; i < fin; i++){
            if(tab[i]> tab[i +1]){
                echanger (tab , i,i+1);
                echange ++;
            }
        }

        fin --;
        for(i=fin; i > deb; i--)
        {
            if(tab[i]< tab[i -1]){
                echanger (tab , i,i -1);
                echange ++;
            }
        }
        deb ++;
    }
}

void echanger (int tab [], int i, int j){
    int tmp;
    tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}

void algo2(int tab [], int nb ){
    int* cpt;
    int min = tab [0] , max = tab [0];
    int i,j;
    for(i=1; i<nb; i++){
        if(min > tab[i]) min = tab[i];
        if(max < tab[i]) max = tab[i];
    }
    cpt = (int *) calloc ((max -min +1) , sizeof (int ));
    if(cpt == NULL ){
        printf ("pb aloc mémoire \n");
        exit (1);
    }
    for(i = 0; i<nb; i++){
        cpt[tab[i]-min ]++;
    }

    j=0;
    for(i=0; i< max -min +1 ; i++){
        while (cpt[i ]!=0){
            tab[j] = i+min;
            j++;
            cpt[i]--;
        }
    }
    free(cpt );
}

void algo3 (int tab [], int n){
    int i, pos , j, tmp;
    for(i = 1; i< n; i++){
        pos = recherchePos (tab ,i, tab[i]);
        if(pos !=i){
            tmp = tab[i];
            for(j = i; j> pos; j--){
                tab[j] = tab[j -1];
            }
            tab[pos] = tmp;
        }
    }
}

int recherchePos (int tab [], int n, int val ){
    int i;
    for(i = 0; i<n; i++){
        if(val < tab[i]){
            return i;
        }
    }
    return i;
}