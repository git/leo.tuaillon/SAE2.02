<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/styles/default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/highlight.min.js"></script>
<script>hljs.highlightAll();</script>

Les structures de données que nous avons choisies pour représenter votre réseau routier sont les suivantes :

1. Point caractéristique :
   - Représenté par la classe `CharacteristicPoint`
   - Attributs :
     - `itsName` : un `std::string` pour stocker le nom du point caractéristique
```cpp
class CharacteristicPoint
{
    private:
    std::string itsName;
    public:
    CharacteristicPoint(std::string name);
    std::string getName() const;
};
```

2. Rue :
- Représenté par la classe `Street`
- Attributs :
  - `StartingPoint` : un pointeur vers un objet `CharacteristicPoint` représentant le point de départ de la rue
  - `ArrivalPoint` : un pointeur vers un objet `CharacteristicPoint` représentant le point d'arrivée de la rue
  - `isOneWay` : un booléen représentant si la rue est à sens unique ou non

```cpp
class Street
{
    private:
    CharacteristicPoint *StartingPoint;
    CharacteristicPoint *ArrivalPoint;
    bool isOneWay;
    public:
    Street(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP, bool oneWay);
    CharacteristicPoint *getStartingPoint() const;
    CharacteristicPoint *getArrivalPoint() const;
    bool getIsOneWay() const;
};
```


3. Réseau routier :
- Représenté par la classe `RoadNetwork`
- Attributs :
  - `itsCharacteristicPoints` : un pointeur vers une liste (`std::list`) d'objets `CharacteristicPoint`, représentant les points caractéristiques du réseau
  - `itsStreets` : un pointeur vers une liste (`std::list`) d'objets `Street`, représentant les rues du réseau
  
```cpp
class RoadNetwork
{
    private:
    std::list<CharacteristicPoint *> *itsCharacteristicPoints;
    std::list<Street *> *itsStreets;
    public:
    RoadNetwork();
    ~RoadNetwork();
    void addCharacteristicPoint(CharacteristicPoint *point);
    void addStreet(Street *street);
    bool isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const;
    bool isAccessible(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const;
    bool fastestRoad(CharacteristicPoint *startingP, CharacteristicPoint arrivalP, std::list<CharacteristicPoint> *way);
};
```

Ces structures de données sont adaptées pour représenter un réseau routier urbain. Elles permettent de stocker les informations pertinentes pour chaque élément (points caractéristiques, rues) et d'effectuer des opérations sur le réseau routier.


La structure de données choisie pour représenter le réseau routier est composée de trois classes principales : `CharacteristicPoint`, `Street` et `RoadNetwork`. Ce choix présente plusieurs avantages en termes de gestion de la mémoire et de temps d'exécution.

1. **Place mémoire** : Les classes `CharacteristicPoint` et `Street` sont conçues pour être légères en mémoire. Elles contiennent uniquement les informations essentielles pour représenter un point caractéristique et une rue, respectivement. De plus, la classe `RoadNetwork` utilise des listes pour stocker les points caractéristiques et les rues, ce qui permet une allocation dynamique de la mémoire en fonction du nombre d'éléments dans le réseau routier. Les listes sont également plus efficaces en termes de mémoire par rapport aux tableaux statiques car elles n'allouent pas d'espace inutilisé.

2. **Temps d'exécution** : Les méthodes de la classe `RoadNetwork` sont optimisées pour offrir de bonnes performances en termes de temps d'exécution. Par exemple, la méthode `isAccessible` utilise une approche de recherche en largeur pour déterminer si un point caractéristique est accessible depuis un autre point. Cette approche est généralement plus rapide que la recherche en profondeur pour ce type de problème. De plus, la méthode `fastestRoad` utilise des fonctions lambda et des listes pour parcourir efficacement les nœuds du réseau et trouver le chemin le plus court entre deux points caractéristiques.

En résumé, le choix des structures de données et des algorithmes dans ce code vise à offrir une bonne gestion de la mémoire et des performances en termes de temps d'exécution.


L'algorithme pour déterminer si le point B est accessible directement depuis le point A en suivant une rue est le suivant :

1. Pour chaque rue dans la liste des rues du réseau routier :
   1. Si le point de départ de la rue est A et le point d'arrivée est B :
      1. --> Retourner Vrai
1. Sinon
    1. --> Retourner Faux

Ce pseudo-code correspond à la méthode `isAccessibleIn1Road` de la classe `RoadNetwork` : 

```cpp
bool RoadNetwork::isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const {
    for (Street *street : *(this->itsStreets))
    {
        if (street->getStartingPoint() == startingP && street->getArrivalPoint() == arrivalP)
        {
            return true;
        }
    }
    return false;
}
```

L'algorithme pour déterminer si le point B est accessible depuis le point A en utilisant une rue ou une succession de rues (en tenant compte du sens de circulation) est le suivant :

1. Créer une liste "à visiter" et ajouter le point A dedans
2. Créer une liste de points "visités"
3. Tant que la liste "à visiter" n'est pas vide :
   1. Prendre le premier point "point_courant" dans la liste "à visiter" et le retirer de la liste
   2. Ajouter "point_courant" à la liste des points "visités"
   3. Pour chaque rue dans la liste des rues du réseau routier :
      1. Si le point de départ de la rue est "point_courant" et le point d'arrivée n'est pas dans la liste "visités" :
         1. Si le point d'arrivée est le point B :
            1. Retourner Vrai
         2. Ajouter le point d'arrivée à la liste "à visiter"
4. Retourner Faux

Ce pseudo-code correspond à la méthode `isAccessible` de la classe `RoadNetwork` : 

```cpp
bool RoadNetwork::isAccessible(CharacteristicPoint *startingP, CharacteristicPoint* arrivalP) const
{
    list<CharacteristicPoint *> *toVisit = new list<CharacteristicPoint *>();
    list<CharacteristicPoint *> *visited = new list<CharacteristicPoint *>();

    toVisit->push_back(startingP);

    while (!toVisit->empty())
    {
        CharacteristicPoint *point = toVisit->front();
        toVisit->pop_front();
        visited->push_back(point);

        for (Street *street : *(this->itsStreets))
        {
            if (street->getStartingPoint() == point &&
                    (find(visited->begin(), visited->end(), street->getArrivalPoint()) == visited->end()))
            {
                if (street->getArrivalPoint() == arrivalP)
                {
                    delete toVisit;
                    delete visited;
                    return true;
                }
                toVisit->push_back(street->getArrivalPoint());
            }
        }
    }

    delete toVisit;
    delete visited;
    return false;
}
```
### Algorithme 3 : Accessibilité directe entre A et B

La complexité de cet algorithme est principalement déterminée par la boucle qui parcourt la liste des rues. Analysons en détail les opérations effectuées dans cet algorithme :

1. Pour chaque rue dans la liste des rues du réseau routier (M rues) :
   1. Vérifier si le point de départ est A et le point d'arrivée est B (2 comparaisons)
   2. Si la condition est remplie, retourner Vrai (1 opération)

Dans le pire des cas, nous devons parcourir l'ensemble des rues (M rues) pour déterminer si B est accessible directement depuis A.

Voici la décomposition des opérations pour cet algorithme :
- 2 comparaisons pour chaque rue (soit 2 * M comparaisons)
- 1 opération de retour si la condition est remplie (au pire des cas, 1 opération)

La complexité de cet algorithme est donc O(M), car nous parcourons toutes les rues une fois et les opérations effectuées dans la boucle sont constantes (ne dépendent pas de la taille des entrées).

Cette complexité est une complexité temporelle, c'est-à-dire qu'elle décrit le nombre d'opérations effectuées en fonction de la taille des entrées (nombre de rues M). D'autres facteurs, tels que la complexité spatiale, pourraient également être considérés pour analyser l'efficacité de cet algorithme.


### Algorithme 4 : Accessibilité entre A et B en utilisant une ou plusieurs rues

La complexité de cet algorithme est principalement déterminée par la boucle while qui explore les points à visiter et la boucle for qui parcourt la liste des rues. Analysons en détail les opérations effectuées dans cet algorithme :

1. Initialiser la liste des points à visiter avec le point A (1 opération)
2. Tant que la liste des points à visiter n'est pas vide (au pire des cas, N-1 itérations, où N est le nombre de points caractéristiques) :
   1. Extraire le premier point de la liste à visiter (1 opération)
   2. Marquer le point comme visité (1 opération)
   3. Pour chaque rue dans la liste des rues du réseau routier (M rues) :
      1. Vérifier si le point de départ est le point courant et si le point d'arrivée n'a pas été visité (2 comparaisons)
      2. Si le point d'arrivée est B, retourner Vrai (1 opération)
      3. Ajouter le point d'arrivée à la liste des points à visiter (1 opération)

Dans le pire des cas, nous devons parcourir l'ensemble des points caractéristiques (N points) et l'ensemble des rues (M rues) pour déterminer si B est accessible depuis A.

Voici la décomposition des opérations pour cet algorithme :
- 1 opération d'initialisation
- N-1 itérations de la boucle while :
  - 1 opération d'extraction du point courant
  - 1 opération de marquage du point comme visité
  - M itérations de la boucle for :
    - 2 comparaisons pour chaque rue (soit 2 * M comparaisons)
    - 1 opération de retour si la condition est remplie (au pire des cas, 1 opération)
    - 1 opération d'ajout à la liste des points à visiter (au pire des cas, 1 opération)

La complexité de cet algorithme est donc O((N-1) * M), car nous devons parcourir toutes les rues pour chaque point caractéristique (à l'exception de B) dans le pire des cas. Les opérations effectuées dans les boucles dépendent du nombre de points caractéristiques et de rues.

Cette complexité est une complexité temporelle, c'est-à-dire qu'elle décrit le nombre d'opérations effectuées en fonction de la taille des entrées (nombre de points caractéristiques N et nombre de rues M). D'autres facteurs, tels que la complexité spatiale, pourraient également être considérés pour analyser l'efficacité de cet algorithme.

