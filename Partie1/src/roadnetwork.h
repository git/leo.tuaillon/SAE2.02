#ifndef ROADNETWORK_H // if not defined
#define ROADNETWORK_H // define ROADNETWORK_H

#include <list> // include list
#include <vector> // include vector
#include <iostream> // include iostream
#include "characteristicpoint.h" // include characteristicpoint.h
#include "street.h" // include street.h

// class RoadNetwork
class RoadNetwork
{
private:
    std::list<CharacteristicPoint *> *itsCharacteristicPoints; // list of characteristic points
    std::list<Street *> *itsStreets; // list of streets
public:
    RoadNetwork(); // constructor
    ~RoadNetwork(); // destructor
    void addCharacteristicPoint(CharacteristicPoint *point); // add a characteristic point
    void addStreet(Street *street); // add a street
    bool isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const;
    bool isAccessible(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const; // check if a characteristic point is accessible from another
    bool fastestRoad(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP, std::list<CharacteristicPoint*> *way); // check if a characteristic point is accessible from another

};


#endif // ROADNETWORK_H // end if not defined
