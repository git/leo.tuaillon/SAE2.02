#include "roadnetwork.h" // include the header file
#include <algorithm> // for find

using namespace std;

RoadNetwork::RoadNetwork()
    :itsCharacteristicPoints{new list<CharacteristicPoint *>()}, itsStreets{new list<Street *>()}
{}


RoadNetwork::~RoadNetwork()
{
    delete itsCharacteristicPoints;
    delete itsStreets;
}

void RoadNetwork::addCharacteristicPoint(CharacteristicPoint *point)
{
    itsCharacteristicPoints->push_back(point);
}

void RoadNetwork::addStreet(Street *street)
{
    itsStreets->push_back(street);
}

bool RoadNetwork::isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const {
    for (Street *street : *(this->itsStreets))
    {
        if (street->getStartingPoint() == startingP && street->getArrivalPoint() == arrivalP)
        {
            return true;
        }
    }
    return false;
}
bool RoadNetwork::isAccessible(CharacteristicPoint *startingP, CharacteristicPoint* arrivalP) const
{
    list<CharacteristicPoint *> *toVisit = new list<CharacteristicPoint *>();
    list<CharacteristicPoint *> *visited = new list<CharacteristicPoint *>();

    toVisit->push_back(startingP);

    while (!toVisit->empty())
    {
        CharacteristicPoint *point = toVisit->front();
        toVisit->pop_front();
        visited->push_back(point);

        for (Street *street : *(this->itsStreets))
        {
            if (street->getStartingPoint() == point &&
                    (find(visited->begin(), visited->end(), street->getArrivalPoint()) == visited->end()))
            {
                if (street->getArrivalPoint() == arrivalP)
                {
                    delete toVisit;
                    delete visited;
                    return true;
                }
                toVisit->push_back(street->getArrivalPoint());
            }
        }
    }

    delete toVisit;
    delete visited;
    return false;
}



bool RoadNetwork::fastestRoad(CharacteristicPoint *starting, CharacteristicPoint *arrival, list<CharacteristicPoint*> *result) {
    // if the road is not accessible, return false and stop the function
    if (!isAccessible(starting, arrival)) {
        return false;
    }

    // lambda function to check if a point is already visited
    auto isVisited = [](CharacteristicPoint *point, list<CharacteristicPoint *> *visited) -> bool
    {
        if (visited == nullptr)
            return false;

        for (CharacteristicPoint *visitedPoint : *visited)
            if (visitedPoint == point)
                return true;
        return false;
    };

    // lambda function to search the next nodes
    auto searchNodes = [this, &isVisited](CharacteristicPoint *starting, list<CharacteristicPoint *> *visited, list<Street *> *result) -> void
    {
        for (Street *street : *(this->itsStreets))
        {
            if ((street->getStartingPoint() == starting && !isVisited(street->getArrivalPoint(), visited))
                || (street->getArrivalPoint() == starting && !street->getIsOneWay() && !isVisited(street->getStartingPoint(), visited)))
            {
                result->push_back(street);
            }
        }
    };

    list<Street *> *startingNodes = new list<Street *>();
    searchNodes(starting, nullptr, startingNodes);

    // if there is no starting node, return false and stop the function
    if (startingNodes->empty())
        return false;

    list<Street *>::iterator it;

    // Traversal of possible starting nodes
    for (list<Street *>::iterator it = startingNodes->begin(); it != startingNodes->end(); ++it) {
        // Initialization of the path and banned lists for this starting node
        list < CharacteristicPoint * > *path = new list<CharacteristicPoint *>();
        list < CharacteristicPoint * > *banned = new list<CharacteristicPoint *>();
        path->push_back(starting);

        if ((*it)->getStartingPoint() == starting)
            path->push_back((*it)->getArrivalPoint());
        else
            path->push_back((*it)->getStartingPoint());

        // While the arrival point has not been reached and the banned list is not full
        while (path->back() != arrival && banned->size() < this->itsCharacteristicPoints->size() - 2) {
            // Search for possible next nodes for the last point of the current path
            list < Street * > *nextNodes = new list<Street *>();
            searchNodes(path->back(), path, nextNodes);

            // Check if a next node has been found
            bool foundNextNode = false;

            //Traverse candidate streets to find a next node
            for (Street *street: *nextNodes) {
                if (!isVisited(street->getStartingPoint(), banned) && !isVisited(street->getArrivalPoint(), banned)) {
                    if (street->getStartingPoint() == path->back()) {
                        path->push_back(street->getArrivalPoint());
                    } else {
                        path->push_back(street->getStartingPoint());
                    }
                    foundNextNode = true;
                    break;
                }
            }

            // If no next node has been found, add the last point of the path to the list of banned points and remove it from the path
            if (!foundNextNode) {
                banned->push_back(path->back());
                path->pop_back();
            }

            // Freeing the memory allocated for nextNodes
            delete nextNodes;
        }

        //If the last point of the path is the destination point and the length of the path is smaller than the current result, update the result"
        if (path->back() == arrival && (path->size() < result->size() || result->empty())) {
            *result = *path;
        }

        // Freeing the memory allocated for path and banned
        delete path;
        delete banned;
    }

    // If there is no path, return false and stop the function
    if (result->empty()) {
        return false;
    }

    // Freeing the memory allocated for startingNodes
    delete startingNodes;
    return true;
}