#include "roadnetwork.h"

void printIsAccessibleIn1Road(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP);
void printIsAccessible(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP);
void printFastestPath(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP);
