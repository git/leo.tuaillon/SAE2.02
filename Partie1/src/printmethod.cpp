#include "printmethod.h"

using namespace std;

void printIsAccessibleIn1Road(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP)
{
    cout << "is ";
    cout << "\033[34m" << arrivalP->getName() << "\033[0m"; // Affiche arrivalP en bleu
    cout << " accessible in 1 road from ";
    cout << "\033[34m" << startingP->getName() << "\033[0m"; // Affiche startingP en bleu
    cout << " ? : ";
    if (roadNetwork->isAccessibleIn1Road(startingP, arrivalP)) {
        cout << "\033[32mYes\033[0m" << endl; // Affiche le "Yes" en vert
    } else {
        cout << "\033[31mNo\033[0m" << endl; // Affiche le "No" en rouge
    }
}

void printIsAccessible(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP)
{
    cout << "is ";
    cout << "\033[34m" << arrivalP->getName() << "\033[0m"; // Affiche arrivalP en bleu
    cout << " accessible from ";
    cout << "\033[34m" << startingP->getName() << "\033[0m"; // Affiche startingP en bleu
    cout << " ? : ";
    if (roadNetwork->isAccessible(startingP, arrivalP)) {
        cout << "\033[32mYes\033[0m" << endl; // Affiche le "Yes" en vert
    } else {
        cout << "\033[31mNo\033[0m" << endl; // Affiche le "No" en rouge
    };
}

void printFastestPath(RoadNetwork *roadNetwork, CharacteristicPoint *startingP, CharacteristicPoint *arrivalP)
{
    list<CharacteristicPoint*> result;
    bool test = roadNetwork->fastestRoad(startingP, arrivalP, &result);

    if(!test) {
        cout << "\033[31mError : \033[0m";
        cout << "No path found from ";
        cout << "\033[34m"<<startingP->getName() << "\033[0m";
        cout << " to ";
        cout << "\033[34m"<<arrivalP->getName() << "\033[0m" << endl;
    }
    else{
        cout << "Fastest path from ";
        cout <<"\033[34m" << startingP->getName() << "\033[0m";
        cout << " to ";
        cout << "\033[34m"<< arrivalP->getName() << "\033[0m";
        cout << " : \t\t";

        for (list<CharacteristicPoint*>::iterator it = result.begin(); it != result.end(); it++) {
            if (it != result.begin()) {
                cout << "\033[32m" << " -> " << "\033[0m";
            }
            cout << "\033[34m" << (*it)->getName() <<  "\033[0m";
        }
        cout << endl;
    }
}