#ifndef STREET_H
#define STREET_H

#include "characteristicpoint.h"

class Street
{
private:
    CharacteristicPoint *StartingPoint; // starting point of the street
    CharacteristicPoint *ArrivalPoint; // arrival point of the street
    bool isOneWay; // true if the street is one way
public:
    Street(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP, bool oneWay); // constructor
    CharacteristicPoint *getStartingPoint() const; // getter for itsStarting
    CharacteristicPoint *getArrivalPoint() const; // getter for itsArrival
    bool getIsOneWay() const; // getter for isOneWay
};

#endif // STREET_H
