#include <iostream>
#include "roadnetwork.h"
#include "printmethod.h"

using namespace std;
void testIsAccessible(){
    RoadNetwork *roadNetwork = new RoadNetwork();

    CharacteristicPoint *A = new CharacteristicPoint{"A"};
    CharacteristicPoint *B = new CharacteristicPoint{"B"};
    CharacteristicPoint *C = new CharacteristicPoint{"C"};
    CharacteristicPoint *D = new CharacteristicPoint{"D"};
    CharacteristicPoint *E = new CharacteristicPoint{"E"};

    roadNetwork->addCharacteristicPoint(A);
    roadNetwork->addCharacteristicPoint(B);
    roadNetwork->addCharacteristicPoint(C);
    roadNetwork->addCharacteristicPoint(D);
    roadNetwork->addCharacteristicPoint(E);

    Street *street1 = new Street{A,C, false};
    Street *street2 = new Street{B, C, false};
    Street *street3 = new Street{C, D, true};
    Street *street4 = new Street{D, E, true};
    //Street *street5 = new Street{E, A, false};

    roadNetwork->addStreet(street1);
    roadNetwork->addStreet(street2);
    roadNetwork->addStreet(street3);
    roadNetwork->addStreet(street4);
    //roadNetwork->addStreet(street5);
    cout << "\n\033[32m=========== Test isAccessible in 1 road : ============\033[0m" << endl;
    printIsAccessibleIn1Road(roadNetwork, A, B);
    printIsAccessibleIn1Road(roadNetwork, A, C);
    printIsAccessibleIn1Road(roadNetwork, A, D);
    printIsAccessibleIn1Road(roadNetwork, A, E);
    printIsAccessibleIn1Road(roadNetwork, B, A);
    printIsAccessibleIn1Road(roadNetwork, B, C);
    printIsAccessibleIn1Road(roadNetwork, B, D);
    printIsAccessibleIn1Road(roadNetwork, E, A);
    printIsAccessibleIn1Road(roadNetwork, E, B);

    cout << "\n\033[32m=========== Test isAccessible : ============\033[0m" << endl;
    printIsAccessible(roadNetwork, A, B);
    printIsAccessible(roadNetwork, A, C);
    printIsAccessible(roadNetwork, A, D);
    printIsAccessible(roadNetwork, A, E);
    printIsAccessible(roadNetwork, B, A);
    printIsAccessible(roadNetwork, B, C);
    printIsAccessible(roadNetwork, B, D);
    printIsAccessible(roadNetwork, E, A);
    printIsAccessible(roadNetwork, E, B);
    printIsAccessible(roadNetwork, E, C);
    printIsAccessible(roadNetwork, E, D);
    printIsAccessible(roadNetwork, E, E);
}

void testFastestRoad(){
    cout << "\n\033[32m========= Test fastestRoad : =========\033[0m" << endl;
    RoadNetwork *roadNetwork = new RoadNetwork();

    CharacteristicPoint *A = new CharacteristicPoint{"A"};
    CharacteristicPoint *B = new CharacteristicPoint{"B"};
    CharacteristicPoint *C = new CharacteristicPoint{"C"};
    CharacteristicPoint *D = new CharacteristicPoint{"D"};
    CharacteristicPoint *E = new CharacteristicPoint{"E"};
    CharacteristicPoint *F = new CharacteristicPoint{"F"};
    CharacteristicPoint *G = new CharacteristicPoint{"G"};

    roadNetwork->addCharacteristicPoint(A);
    roadNetwork->addCharacteristicPoint(B);
    roadNetwork->addCharacteristicPoint(C);
    roadNetwork->addCharacteristicPoint(D);
    roadNetwork->addCharacteristicPoint(E);
    roadNetwork->addCharacteristicPoint(F);
    roadNetwork->addCharacteristicPoint(G);

    Street *street1 = new Street{A,B, false};
    Street *street2 = new Street{B, C, false};
    Street *street3 = new Street{C, D, false};
    Street *street4 = new Street{B, E, false};
    Street *street5 = new Street{E, F, false};
    Street *street6 = new Street{F, G, true};
    Street *street7 = new Street{C, G, false};

    roadNetwork->addStreet(street1);
    roadNetwork->addStreet(street2);
    roadNetwork->addStreet(street3);
    roadNetwork->addStreet(street4);
    roadNetwork->addStreet(street5);
    roadNetwork->addStreet(street6);
    roadNetwork->addStreet(street7);

    printFastestPath(roadNetwork, A, B);
    printFastestPath(roadNetwork, A, C);
    printFastestPath(roadNetwork, A, D);
    printFastestPath(roadNetwork, A, E);
    printFastestPath(roadNetwork, A, F);
    printFastestPath(roadNetwork, A, G);
    printFastestPath(roadNetwork, B, A);
    printFastestPath(roadNetwork, B, C);
    printFastestPath(roadNetwork, B, D);
    printFastestPath(roadNetwork, B, E);
    printFastestPath(roadNetwork, B, F);
    printFastestPath(roadNetwork, B, G);
    printFastestPath(roadNetwork, C, A);
    printFastestPath(roadNetwork, C, B);
    printFastestPath(roadNetwork, C, D);
    printFastestPath(roadNetwork, C, E);

    // todo : redefinir l'opérator pour une liste de point caractéristique

    //roadNetwork->printPath(resultBG);
    //printFastestPath(resultAB);

    delete roadNetwork;

    delete A;
    delete B;
    delete C;
    delete D;
    delete E;
    delete F;
    delete G;

    delete street1;
    delete street2;
    delete street3;
    delete street4;
    delete street5;
    delete street6;
    delete street7;

}

void testBigResaux(){
    RoadNetwork *roadNetwork = new RoadNetwork();

    CharacteristicPoint *A = new CharacteristicPoint{"A"};
    CharacteristicPoint *B = new CharacteristicPoint{"B"};
    CharacteristicPoint *C = new CharacteristicPoint{"C"};
    CharacteristicPoint *D = new CharacteristicPoint{"D"};
    CharacteristicPoint *E = new CharacteristicPoint{"E"};
    CharacteristicPoint *F = new CharacteristicPoint{"F"};
    CharacteristicPoint *G = new CharacteristicPoint{"G"};
    CharacteristicPoint *H = new CharacteristicPoint{"H"};
    CharacteristicPoint *I = new CharacteristicPoint{"I"};
    CharacteristicPoint *J = new CharacteristicPoint{"J"};
    CharacteristicPoint *K = new CharacteristicPoint{"K"};
    CharacteristicPoint *L = new CharacteristicPoint{"L"};
    CharacteristicPoint *M = new CharacteristicPoint{"M"};
    CharacteristicPoint *N = new CharacteristicPoint{"N"};
    CharacteristicPoint *O = new CharacteristicPoint{"O"};
    CharacteristicPoint *P = new CharacteristicPoint{"P"};
    CharacteristicPoint *Q = new CharacteristicPoint{"Q"};
    CharacteristicPoint *R = new CharacteristicPoint{"R"};
    CharacteristicPoint *S = new CharacteristicPoint{"S"};
    CharacteristicPoint *T = new CharacteristicPoint{"T"};
    CharacteristicPoint *U = new CharacteristicPoint{"U"};
    CharacteristicPoint *V = new CharacteristicPoint{"V"};
    CharacteristicPoint *W = new CharacteristicPoint{"W"};
    CharacteristicPoint *X = new CharacteristicPoint{"X"};
    CharacteristicPoint *Y = new CharacteristicPoint{"Y"};
    CharacteristicPoint *Z = new CharacteristicPoint{"Z"};
    CharacteristicPoint *AA = new CharacteristicPoint{"AA"};
    CharacteristicPoint *BB = new CharacteristicPoint{"BB"};
    CharacteristicPoint *CC = new CharacteristicPoint{"CC"};
    CharacteristicPoint *DD = new CharacteristicPoint{"DD"};
    CharacteristicPoint *EE = new CharacteristicPoint{"EE"};
    CharacteristicPoint *FF = new CharacteristicPoint{"FF"};
    CharacteristicPoint *GG = new CharacteristicPoint{"GG"};

    roadNetwork->addCharacteristicPoint(A);
    roadNetwork->addCharacteristicPoint(B);
    roadNetwork->addCharacteristicPoint(C);
    roadNetwork->addCharacteristicPoint(D);
    roadNetwork->addCharacteristicPoint(E);
    roadNetwork->addCharacteristicPoint(F);
    roadNetwork->addCharacteristicPoint(G);
    roadNetwork->addCharacteristicPoint(H);
    roadNetwork->addCharacteristicPoint(I);
    roadNetwork->addCharacteristicPoint(J);
    roadNetwork->addCharacteristicPoint(K);
    roadNetwork->addCharacteristicPoint(L);
    roadNetwork->addCharacteristicPoint(M);
    roadNetwork->addCharacteristicPoint(N);
    roadNetwork->addCharacteristicPoint(O);
    roadNetwork->addCharacteristicPoint(P);
    roadNetwork->addCharacteristicPoint(Q);
    roadNetwork->addCharacteristicPoint(R);
    roadNetwork->addCharacteristicPoint(S);
    roadNetwork->addCharacteristicPoint(T);
    roadNetwork->addCharacteristicPoint(U);
    roadNetwork->addCharacteristicPoint(V);
    roadNetwork->addCharacteristicPoint(W);
    roadNetwork->addCharacteristicPoint(X);
    roadNetwork->addCharacteristicPoint(Y);
    roadNetwork->addCharacteristicPoint(Z);
    roadNetwork->addCharacteristicPoint(AA);
    roadNetwork->addCharacteristicPoint(BB);
    roadNetwork->addCharacteristicPoint(CC);
    roadNetwork->addCharacteristicPoint(DD);
    roadNetwork->addCharacteristicPoint(EE);
    roadNetwork->addCharacteristicPoint(FF);
    roadNetwork->addCharacteristicPoint(GG);

    Street *street1 = new Street{A,B, false};
    Street *street2 = new Street{B, C, false};
    Street *street3 = new Street{C, D, false};
    Street *street4 = new Street{B, E, false};
    Street *street5 = new Street{E, F, false};
    Street *street6 = new Street{F, G, true};
    Street *street7 = new Street{C, G, false};
    Street *street8 = new Street{G, H, false};
    Street *street9 = new Street{H, I, false};
    Street *street10 = new Street{I, J, false};
    Street *street11 = new Street{J, K, false};
    Street *street12 = new Street{K, L, true};
    Street *street13 = new Street{L, M, false};
    Street *street14 = new Street{M, N, false};
    Street *street15 = new Street{N, O, false};
    Street *street16 = new Street{O, P, false};
    Street *street17 = new Street{P, Q, false};
    Street *street18 = new Street{Q, R, false};
    Street *street19 = new Street{R, S, true};
    Street *street20 = new Street{S, T, false};
    Street *street21 = new Street{T, U, false};
    Street *street22 = new Street{U, V, true};
    Street *street23 = new Street{V, W, false};
    Street *street24 = new Street{W, X, false};
    Street *street25 = new Street{X, Y, false};
    Street *street26 = new Street{Y, Z, false};
    Street *street27 = new Street{Z, AA, false};
    Street *street28 = new Street{AA, BB, true};
    Street *street29 = new Street{BB, CC, false};
    Street *street30 = new Street{CC, DD, false};
    Street *street31 = new Street{DD, EE, false};
    Street *street32 = new Street{EE, FF, false};
    Street *street33 = new Street{FF, GG, false};
    Street *street34 = new Street{GG, H, false};
    Street *street35 = new Street{GG, I, true};
    Street *street36 = new Street{GG, J, false};
    Street *street37 = new Street{GG, K, false};
    Street *street38 = new Street{GG, L, false};
    Street *street39 = new Street{GG, M, false};

    roadNetwork->addStreet(street1);
    roadNetwork->addStreet(street2);
    roadNetwork->addStreet(street3);
    roadNetwork->addStreet(street4);
    roadNetwork->addStreet(street5);
    roadNetwork->addStreet(street6);
    roadNetwork->addStreet(street7);
    roadNetwork->addStreet(street8);
    roadNetwork->addStreet(street9);
    roadNetwork->addStreet(street10);
    roadNetwork->addStreet(street11);
    roadNetwork->addStreet(street12);
    roadNetwork->addStreet(street13);
    roadNetwork->addStreet(street14);
    roadNetwork->addStreet(street15);
    roadNetwork->addStreet(street16);
    roadNetwork->addStreet(street17);
    roadNetwork->addStreet(street18);
    roadNetwork->addStreet(street19);
    roadNetwork->addStreet(street20);
    roadNetwork->addStreet(street21);
    roadNetwork->addStreet(street22);
    roadNetwork->addStreet(street23);
    roadNetwork->addStreet(street24);
    roadNetwork->addStreet(street25);
    roadNetwork->addStreet(street26);
    roadNetwork->addStreet(street27);
    roadNetwork->addStreet(street28);
    roadNetwork->addStreet(street29);
    roadNetwork->addStreet(street30);
    roadNetwork->addStreet(street31);
    roadNetwork->addStreet(street32);
    roadNetwork->addStreet(street33);
    roadNetwork->addStreet(street34);
    roadNetwork->addStreet(street35);
    roadNetwork->addStreet(street36);
    roadNetwork->addStreet(street37);
    roadNetwork->addStreet(street38);
    roadNetwork->addStreet(street39);

    printIsAccessible(roadNetwork, A, A);
    printIsAccessible(roadNetwork, A, B);
    printIsAccessible(roadNetwork, A, C);
    printIsAccessible(roadNetwork, O, D);
    printIsAccessible(roadNetwork, A, E);
    printIsAccessible(roadNetwork, A, F);
    printIsAccessible(roadNetwork, I, G);
    printIsAccessible(roadNetwork, A, H);
    printIsAccessible(roadNetwork, A, I);
    printIsAccessible(roadNetwork, G, J);
    printIsAccessible(roadNetwork, A, K);
    printIsAccessible(roadNetwork, A, L);
    printIsAccessible(roadNetwork, F, M);
    printIsAccessible(roadNetwork, A, N);
    printIsAccessible(roadNetwork, A, O);
    printIsAccessible(roadNetwork, E, P);
    printIsAccessible(roadNetwork, A, Q);
    printIsAccessible(roadNetwork, A, AA);
    printIsAccessible(roadNetwork, AA, S);
    printIsAccessible(roadNetwork, A, T);

    printFastestPath(roadNetwork, A, A);
    printFastestPath(roadNetwork, A, B);
    printFastestPath(roadNetwork, A, C);
    printFastestPath(roadNetwork, O, D);
    printFastestPath(roadNetwork, A, E);
    printFastestPath(roadNetwork, A, F);
    printFastestPath(roadNetwork, I, G);
    printFastestPath(roadNetwork, A, H);
    printFastestPath(roadNetwork, A, I);
    printFastestPath(roadNetwork, G, J);
    printFastestPath(roadNetwork, A, K);
    printFastestPath(roadNetwork, A, L);
    printFastestPath(roadNetwork, F, M);
    printFastestPath(roadNetwork, A, N);
    printFastestPath(roadNetwork, A, O);
    printFastestPath(roadNetwork, E, P);
    printFastestPath(roadNetwork, A, Q);
    printFastestPath(roadNetwork, A, AA);
    printFastestPath(roadNetwork, AA, S);
    printFastestPath(roadNetwork, A, T);


}
int main()
{

    testIsAccessible();
    testFastestRoad();
    //testBigResaux();
    return 0;

}