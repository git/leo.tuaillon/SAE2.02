#include "street.h"
// constructor
Street::Street(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP, bool oneWay)
    :StartingPoint{startingP}, ArrivalPoint{arrivalP}, isOneWay{oneWay}
{}

// getter for itsStarting
CharacteristicPoint *Street::getStartingPoint() const
{
    return StartingPoint;
}

// getter for itsArrival
CharacteristicPoint *Street::getArrivalPoint() const
{
    return ArrivalPoint;
}

// getter for isOneWay
bool Street::getIsOneWay() const
{
    return isOneWay;
}
