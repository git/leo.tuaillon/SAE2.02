#include "characteristicpoint.h"

using namespace std;
// constructor
CharacteristicPoint::CharacteristicPoint(string name)
    :itsName{name}
{}

// getter for itsName
string CharacteristicPoint::getName() const
{
    return itsName;
}
