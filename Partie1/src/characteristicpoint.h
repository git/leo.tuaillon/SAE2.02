#ifndef CHARACTERISTICPOINT_H
#define CHARACTERISTICPOINT_H

#include <string>

class CharacteristicPoint
{
private:
    std::string itsName; // name of the characteristic point
public:
    CharacteristicPoint(std::string name); // constructor
    std::string getName() const; // getter for itsName
};

#endif // CHARACTERISTICPOINT_H
