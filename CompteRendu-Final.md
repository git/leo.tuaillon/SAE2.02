<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/styles/default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.2.0/highlight.min.js"></script>
<script>hljs.highlightAll();</script>

# Compte Rendu de la Partie 1 de la SAE2.02

Les structures de données que nous avons choisies pour représenter votre réseau routier sont les suivantes :

1. Point caractéristique :
   - Représenté par la classe `CharacteristicPoint`
   - Attributs :
     - `itsName` : un `std::string` pour stocker le nom du point caractéristique
```cpp
class CharacteristicPoint
{
    private:
    std::string itsName;
    public:
    CharacteristicPoint(std::string name);
    std::string getName() const;
};
```

2. Rue :
- Représenté par la classe `Street`
- Attributs :
  - `StartingPoint` : un pointeur vers un objet `CharacteristicPoint` représentant le point de départ de la rue
  - `ArrivalPoint` : un pointeur vers un objet `CharacteristicPoint` représentant le point d'arrivée de la rue
  - `isOneWay` : un booléen représentant si la rue est à sens unique ou non

```cpp
class Street
{
    private:
    CharacteristicPoint *StartingPoint;
    CharacteristicPoint *ArrivalPoint;
    bool isOneWay;
    public:
    Street(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP, bool oneWay);
    CharacteristicPoint *getStartingPoint() const;
    CharacteristicPoint *getArrivalPoint() const;
    bool getIsOneWay() const;
};
```


3. Réseau routier :
- Représenté par la classe `RoadNetwork`
- Attributs :
  - `itsCharacteristicPoints` : un pointeur vers une liste (`std::list`) d'objets `CharacteristicPoint`, représentant les points caractéristiques du réseau
  - `itsStreets` : un pointeur vers une liste (`std::list`) d'objets `Street`, représentant les rues du réseau
  
```cpp
class RoadNetwork
{
    private:
    std::list<CharacteristicPoint *> *itsCharacteristicPoints;
    std::list<Street *> *itsStreets;
    public:
    RoadNetwork();
    ~RoadNetwork();
    void addCharacteristicPoint(CharacteristicPoint *point);
    void addStreet(Street *street);
    bool isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const;
    bool isAccessible(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const;
    bool fastestRoad(CharacteristicPoint *startingP, CharacteristicPoint arrivalP, std::list<CharacteristicPoint> *way);
};
```

Ces structures de données sont adaptées pour représenter un réseau routier urbain. Elles permettent de stocker les informations pertinentes pour chaque élément (points caractéristiques, rues) et d'effectuer des opérations sur le réseau routier.


La structure de données choisie pour représenter le réseau routier est composée de trois classes principales : `CharacteristicPoint`, `Street` et `RoadNetwork`. Ce choix présente plusieurs avantages en termes de gestion de la mémoire et de temps d'exécution.

1. **Place mémoire** : Les classes `CharacteristicPoint` et `Street` sont conçues pour être légères en mémoire. Elles contiennent uniquement les informations essentielles pour représenter un point caractéristique et une rue. De plus, la classe `RoadNetwork` utilise des listes pour stocker les points caractéristiques et les rues, ce qui permet une allocation dynamique de la mémoire en fonction du nombre d'éléments dans le réseau routier. 
Les listes sont également plus efficaces en termes de mémoire par rapport aux tableaux statiques car elles n'allouent pas d'espace inutilisé.

2. **Temps d'exécution** : Les méthodes de la classe `RoadNetwork` sont optimisées pour offrir de bonnes performances en termes de temps d'exécution. Par exemple, la méthode `isAccessible` utilise une recherche en regardant chaque rues partant d'un point donné afin de déterminer si un point caractéristique est accessible depuis un autre point. 
Cette approche est généralement plus rapide que la recherche en profondeur pour ce type de problème.

En résumé, le choix des structures de données et des algorithmes dans ce code vise à offrir une bonne gestion de la mémoire et des performances en termes de temps d'exécution.


L'algorithme pour déterminer si le point B est accessible directement depuis le point A en suivant une rue est le suivant :

1. Pour chaque rue dans la liste des rues du réseau routier :
   1. Si le point de départ de la rue est A et le point d'arrivée est B :
      1. --> Retourner Vrai
1. Sinon
    1. --> Retourner Faux

Ce pseudo-code correspond à la méthode `isAccessibleIn1Road` de la classe `RoadNetwork` : 

```cpp
bool RoadNetwork::isAccessibleIn1Road(CharacteristicPoint *startingP, CharacteristicPoint *arrivalP) const {
    for (Street *street : *(this->itsStreets))
    {
        if (street->getStartingPoint() == startingP && street->getArrivalPoint() == arrivalP)
        {
            return true;
        }
    }
    return false;
}
```

L'algorithme pour déterminer si le point B est accessible depuis le point A en utilisant une rue ou une succession de rues (en tenant compte des sens uniques ) est le suivant :

1. Créer une liste "à visiter" et ajouter le point A dedans
2. Créer une liste de points "visités"
3. Tant que la liste "à visiter" n'est pas vide :
   1. Prendre le premier point "point_courant" dans la liste "à visiter" et le retirer de la liste
   2. Ajouter "point_courant" à la liste des points "visités"
   3. Pour chaque rue dans la liste des rues du réseau routier :
      1. Si le point de départ de la rue est "point_courant" et le point d'arrivée n'est pas dans la liste "visités" :
         1. Si le point d'arrivée est le point B :
            1. Retourner Vrai
         2. Ajouter le point d'arrivée à la liste "à visiter"
4. Retourner Faux

Ce pseudo-code correspond à la méthode `isAccessible` de la classe `RoadNetwork` : 

```cpp
bool RoadNetwork::isAccessible(CharacteristicPoint *startingP, CharacteristicPoint* arrivalP) const
{
    list<CharacteristicPoint *> *toVisit = new list<CharacteristicPoint *>();
    list<CharacteristicPoint *> *visited = new list<CharacteristicPoint *>();

    toVisit->push_back(startingP);

    while (!toVisit->empty())
    {
        CharacteristicPoint *point = toVisit->front();
        toVisit->pop_front();
        visited->push_back(point);

        for (Street *street : *(this->itsStreets))
        {
            if (street->getStartingPoint() == point &&
                    (find(visited->begin(), visited->end(), street->getArrivalPoint()) == visited->end()))
            {
                if (street->getArrivalPoint() == arrivalP)
                {
                    delete toVisit;
                    delete visited;
                    return true;
                }
                toVisit->push_back(street->getArrivalPoint());
            }
        }
    }

    delete toVisit;
    delete visited;
    return false;
}
```
### Algorithme 1 : Accessibilité directe entre A et B

La complexité de cet algorithme est principalement déterminée par la boucle qui parcourt la liste des rues. 
Voici une analyse en détail les opérations effectuées dans cet algorithme :

1. Pour chaque rue dans la liste des rues du réseau routier (M rues) :
   1. Vérifier si le point de départ est A et le point d'arrivée est B (2 comparaisons)
   2. Si la condition est remplie, retourner Vrai (1 opération)

Dans le pire des cas, nous devons parcourir l'ensemble des rues (M rues) pour déterminer si B est accessible directement depuis A.

Voici la décomposition des opérations pour cet algorithme :
- 2 comparaisons pour chaque rue (soit 2 * M comparaisons)
- 1 opération si la condition est remplie (au pire des cas, 1 opération)

La complexité de cet algorithme est donc O(M), car nous parcourons toutes les rues une fois et les opérations effectuées dans la boucle sont constantes (ne dépendent pas de la taille des entrées).

Cette complexité est une complexité temporelle, c'est-à-dire qu'elle décrit le nombre d'opérations effectuées en fonction de la taille des entrées (nombre de rues M). 
D'autres facteurs, tels que la complexité spatiale qui prend en compte l'espace mémoire alloué, pourrait également être considérés pour analyser plus précisément l'efficacité de cet algorithme.


### Algorithme 2 : Accessibilité entre A et B en utilisant une ou plusieurs rues

La complexité de cet algorithme est principalement déterminée par la boucle while qui explore les points à visiter et la boucle for qui parcourt la liste des rues. 
Voilà l'analyse en détail les opérations effectuées dans cet algorithme :

1. Initialiser la liste des points à visiter avec le point A (1 opération)
2. Tant que la liste des points à visiter n'est pas vide (N-1 itérations, où N est le nombre de points caractéristiques) :
   1. Extraire le premier point de la liste à visiter (1 opération)
   2. Marquer le point comme visité (1 opération)
   3. Pour chaque rue dans la liste des rues du réseau routier (M rues) :
      1. Vérifier si le point de départ est le point courant et si le point d'arrivée n'a pas été visité (2 comparaisons)
      2. Si le point d'arrivée est B, retourner Vrai (1 opération)
      3. Ajouter le point d'arrivée à la liste des points à visiter (1 opération)

Dans le pire des cas, nous devons parcourir l'ensemble des points caractéristiques (N points) et l'ensemble des rues (M rues) pour déterminer si B est accessible depuis A.

Voici la décomposition des opérations pour cet algorithme :
- 1 opération d'initialisation
- N-1 itérations de la boucle while :
  - 1 opération d'extraction du point courant
  - 1 opération de marquage du point comme visité
  - M itérations de la boucle for :
    - 2 comparaisons pour chaque rue (soit 2 * M comparaisons)
    - 1 opération de retour si la condition est remplie 
    - 1 opération d'ajout à la liste des points à visiter

La complexité de cet algorithme est donc O((N-1) * M), car nous devons parcourir toutes les rues pour chaque point caractéristique (à l'exception de B) dans le pire des cas. Les opérations effectuées dans les boucles dépendent du nombre de points caractéristiques et de rues.


# COMPTE RENDU DE LA PARTIE 2 DE LA SAE2.02 

## Question 1
### Algo 1 : 
**Schéma du tableau de l'algo 1 déjà trié**
![1.1](screenSchéma/1.1.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 1 trié à l'envers**
![1.1](screenSchéma/1.2.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 1 dans le désordre**
![1.1](screenSchéma/1.3png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
### Algo 2 : 
**Schéma du tableau de l'algo 2 déjà trié**
![1.1](screenSchéma/2.1.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 2 trié à l'envers**
![1.1](screenSchéma/2.2.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 2 dans le désordre**
![1.1](screenSchéma/2.3.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>

### Algo 3 : 
**Schéma du tableau de l'algo 3 déjà trié**
![1.1](screenSchéma/3.1.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 3 trié à l'envers**
![1.1](screenSchéma/3.2.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
<br>
**Schéma du tableau de l'algo 3 dans le désordre**
![1.1](screenSchéma/3.3.png)
Suite du schéma sur le powerpoint en pièce jointe : <span style="color:red;">Attention décalage sous libre office (ouvrir avec PowerPoint)</span>
<br>
## Question 2:

## Compte-rendu de complexité de l'algorithme algo1.

```c
void algo1 (int tab [], int nb ){
    int echange = 1;
    int i;
    int deb = 0, fin = nb -1;

    while (echange >0){
        echange =0;
        for(i=deb; i < fin; i++){
            if(tab[i]> tab[i +1]){
                echanger (tab , i,i+1);
                echange ++;
            }
        }

        fin --;
        for(i=fin; i > deb; i--)
        {
            if(tab[i]< tab[i -1]){
                echanger (tab , i,i -1);
                echange ++;
            }
        }
        deb ++;
    }
}

void echanger (int tab [], int i, int j){
    int tmp;
    tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}
```

# Analyse de la complexité
### Boucle while

La boucle `while` s'exécute tant que la variable `echange` est supérieure à 0. Cette variable est initialisée à 1 avant le début de la boucle, ce qui garantit que la boucle s'exécute au moins une fois.

Le corps de la boucle contient deux boucles `for` qui trient le tableau, l'une en parcourant le tableau de gauche à droite, l'autre en parcourant le tableau de droite à gauche. La complexité de chaque boucle `for` est de O(n), où n est le nombre d'éléments dans le tableau.

Le nombre total d'itérations de la boucle `while` dépend de la disposition des éléments dans le tableau. Dans le pire des cas, le tableau est trié dans l'ordre inverse, ce qui entraîne un nombre maximal d'échanges à chaque itération de la boucle `while`. Dans ce cas, la boucle `while` effectue n itérations (car il y a n éléments dans le tableau) et chaque itération de la boucle `while` effectue deux boucles `for` de O(n) itérations chacune. Ainsi, la complexité totale de la boucle `while` dans le pire des cas est de O(n²).

### Fonction echanger

La fonction `echanger` a une complexité de O(1), car elle effectue un nombre constant d'opérations (3 affectations de variables) quel que soit la taille du tableau.

### Conclusion

La complexité de l'algorithme algo1 dépend de la disposition des éléments dans le tableau. Dans le pire des cas, l'algorithme a une complexité de O(n²). La complexité moyenne est de O(n²), ce qui en fait un algorithme relativement lent pour trier de grands tableaux.

## Compte rendu de l'algorithme algo2

```c
void algo2(int tab [], int nb ){
    int* cpt;
    int min = tab [0] , max = tab [0];
    int i,j;
    for(i=1; i<nb; i++){
        if(min > tab[i]) min = tab[i];
        if(max < tab[i]) max = tab[i];
    }
    cpt = (int *) calloc ((max -min +1) , sizeof (int ));
    if(cpt == NULL ){
        printf ("pb aloc mémoire \n");
        exit (1);
    }
    for(i = 0; i<nb; i++){
        cpt[tab[i]-min ]++;
    }

    j=0;
    for(i=0; i< max -min +1 ; i++){
        while (cpt[i ]!=0){
            tab[j] = i+min;
            j++;
            cpt[i]--;
        }
    }
    free(cpt );
}
```


# Analyse de la complexité :

### Première boucle for
La fonction `algo2` utilise l'algorithme de tri par comptage pour trier un tableau `tab` de taille `nb`. 
Tout d'abord, elle parcourt le tableau pour trouver les valeurs minimales et maximales, ce qui prend une complexité de 
O(n). Ensuite, elle alloue de la mémoire pour un tableau de comptage `cpt` de taille `max-min+1`, ce qui prend une complexité de O(max), où max est la plage de valeurs possibles dans le tableau.

### Deuxième boucle for
Ensuite, elle parcourt le tableau `tab` une deuxième fois pour incrémenter les compteurs correspondants dans le tableau de comptage `cpt`, ce qui prend une complexité de O(n). Enfin, elle parcourt le tableau de comptage cpt pour réinsérer les valeurs triées dans le tableau d'origine tab. Cela prend une complexité de O(max), où max est la plage de valeurs possibles dans le tableau.

### Conclusion
Ainsi, la complexité totale de la fonction `algo2` est de O(n+max), où n est la taille du tableau et max est la plage de valeurs possibles dans le tableau. Le tri par comptage est efficace est efficace pour des tableaux a grande valeurs.

## Compte rendu de l'algorithme algo3
```c
void algo3 (int tab [], int n){
    int i, pos , j, tmp;
    for(i = 1; i< n; i++){
        pos = recherchePos (tab ,i, tab[i]);
        if(pos !=i){
            tmp = tab[i];
            for(j = i; j> pos; j--){
                tab[j] = tab[j -1];
            }
            tab[pos] = tmp;
        }
    }
}

int recherchePos (int tab [], int n, int val ){
    int i;
    for(i = 0; i<n; i++){
        if(val < tab[i]){
            return i;
        }
    }
    return i;
}
```

# Analyse de la complexité:

### Première boucle for :

La boucle principale de "algo3" parcourt le tableau `tab` `n-1` fois, car elle commence à l'indice 1 et se termine à l'indice `n-1`. Elle contient une instruction conditionnelle `if` qui peut être vraie ou fausse. Si elle est vraie, une autre boucle `for` est exécutée, qui parcourt une portion du tableau qui peut aller jusqu'à `i` éléments. En moyenne, cette portion contient `i/2` éléments.

### Boucle for interne :
Lorsque la boucle intérieure est exécutée, la fonction `recherchePos` est appelée pour chaque élément i du tableau, et cette fonction prend un temps proportionnel à i pour s'exécuter. Cela signifie que le temps nécessaire pour exécuter la boucle intérieure dépendra de la taille de la boucle (qui est n-1), et sera donc de l'ordre de grandeur O(1) + O(2) + O(3) + ... + O(n-1).

En d'autres termes, le temps nécessaire pour exécuter la boucle intérieure sera proportionnel à la somme des temps nécessaires pour exécuter chaque itération de la boucle, et cette somme est équivalente à la somme des premiers n-1 entiers, qui est de l'ordre de grandeur O(n²/2) et donc de l'ordre de grandeur O(n²) si on ignore les termes de plus faible ordre.

### Fonction recherchePos :
La fonction `recherchePos` a une complexité de O(n) dans le pire des cas, car elle parcourt le tableau `tab` jusqu'à trouver une position où la valeur `val` doit être insérée.

### Conclusion :
Cela donne un coût temporel de O(n²) pour la boucle intérieure.
La complexité totale de l'algorithme est donc la somme des coûts temporels des deux boucles, soit O(n) + O(n²) = O(n²) dans le pire des cas.

## Conclusion sur la meilleur complexité théorique :
On peut donc conclure que l’algorithme de tri avec la meilleure complexité théorique est l’algo numéro 2, le tri par comptage car avec une complexité de O(n+max) il est bien plus performant avec des tableaux ayant des très grandes valeurs.


## Question 3

![image](Partie2/courbe.png)
Voici la courbe qui représente les différents temps d'éxecution des trois algorithmes.
Ces algorithmes trient différents tableaux de différentes grandeurs.
Nous avons réalisés ces courbes grâce aux données récupérées pour les algos 1, 2 et 3 grâce au test suivant:

```c
void test_1() {
    int nb;
    printf("Entrez le nombre d'éléments du tableau : ");
    scanf("%d", &nb);

    int tab[nb];

    // Génère un tableau d'éléments avec des nombres aléatoires entre 1 et 100
    srand(time(NULL));
    for (int i = 0; i < nb; i++) {
        tab[i] = rand() % 100 + 1;
    }

    clock_t debut = clock(); // Début du chronomètre
    algo1(tab, nb); // Appel de la fonction à tester
    clock_t fin = clock(); // Fin du chronomètre
    double temps_execution = (double)(fin - debut) / CLOCKS_PER_SEC; // Calcul du temps d'exécution

    printf("Temps d'execution : %f secondes\n", temps_execution); // Affichage du temps d'exécution
}
```
Nous les avons ensuite reportées dans un tableau excel. <br>
Nous pouvons voir que c'est l'algorithme 2 qui est le plus rapide en terme de temps d'execution. Et que les deux autres courbes sont exponentielles.

## Question 4 :
## Résumé algo 1: 
L'algorithme `algo1` est un algorithme de tri appelé `Tri Cocktail`. Il s'agit d'une variante du Tri à bulles qui permet de trier un tableau en le parcourant dans les deux sens.

1. Avantages :
    - L'algorithme Tri Cocktail est plus efficace que le Tri à bulles car il peut détecter des éléments inversés dans le tableau dans les deux sens.
    - Cet algorithme peut être plus rapide que d'autres algorithmes de tri pour des tableaux déjà partiellement triés.

2. Inconvénients :
    - Le Tri Cocktail peut être moins performant que d'autres algorithmes de tri pour des tableaux désordonnés.
    - Il peut être plus complexe à implémenter et à comprendre que d'autres algorithmes de tri.
    - De plus sur des tableaux très long, il n’est pas performant du fait de sa complexité de O(n²)

## Résumé algo 2: 
L'algorithme `algo2` est un algorithme de tri appelé `Tri par comptage`. Il consiste à dénombrer le nombre d'occurrences de chaque élément du tableau à trier pour ensuite réécrire le tableau dans l'ordre.

1. Avantages :
    - Le tri par comptage est très efficace pour trier de grandes quantités de données. Il est souvent beaucoup plus rapide que d'autres méthodes de tri, telles que le tri à bulles ou le tri fusion.
2. Inconvénients :
    - Peut être plus complexe à comprendre que d’autres algorithme de tri tel que le tri par insertion 
    - Requiert un espace mémoire suffisant : Le tri par comptage nécessite un espace mémoire suffisant pour stocker les compteurs. 
    - Si l'espace mémoire n'est pas suffisant, le tri par comptage ne peut pas être utilisé pour trier les données.


## Résumé algo 3: 
L'algorithme `algo3` est un algorithme de tri appelé `Tri par insertion`. Il consiste à insérer chaque élément du tableau à sa place dans un sous-tableau trié.

1. Avantages :
    - L'algorithme de tri par insertion est efficace pour les tableaux de petite taille ou déjà partiellement triés.
    - Il est facile à comprendre et à implémenter.
2. Inconvénients :
    - L'algorithme de tri par insertion peut être inefficace pour les tableaux de grande taille ou désordonnés.
    - Du fait de sa complexité temporelle de O(n²), il peut être très lent à trier pour les tableaux de grande taille.